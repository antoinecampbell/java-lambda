-injars       build/distributions/app.zip
-outjars      build/distributions/app-processed.zip
-libraryjars  <java.home>/lib/rt.jar

-dontoptimize
-dontobfuscate

-dontwarn **

-printseeds build/seeds.txt
-printusage build/usage.txt
-printmapping build/mapping.txt

#-dump build/dump.txt

# Preserve all annotations.
-keepattributes *Annotation*

# Keep all classes from package
-keep class com.antoine.campbell.lambda.** {
    *;
}

# Keep jackson
-keep class com.fasterxml.jackson.** {
    *;
}

