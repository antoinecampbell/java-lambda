provider "aws" {
  region = "${var.region}"
}

variable "region" {
  default = "us-east-1"
}

resource "aws_iam_role" "test" {
  name               = "test-lambda-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "test-processed" {
  function_name = "test-processed"
  handler       = "com.antoine.campbell.lambda.Handler::handleRequest"
  runtime       = "java8"
  filename      = "../build/distributions/app-processed.zip"
  role          = "${aws_iam_role.test.arn}"

  timeout     = 15
  memory_size = 128
}

resource "aws_lambda_function" "test" {
  function_name = "test"
  handler       = "com.antoine.campbell.lambda.Handler::handleRequest"
  runtime       = "java8"
  filename      = "../build/distributions/app.zip"
  role          = "${aws_iam_role.test.arn}"

  timeout     = 15
  memory_size = 128
}