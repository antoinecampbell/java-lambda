package com.antoine.campbell.lambda;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

/**
 * POJO containing response object for API Gateway.
 */
@AllArgsConstructor
@Getter
public class GatewayResponse {

    private final String body;
    private final Map<String, String> headers;
    private final int statusCode;
}
